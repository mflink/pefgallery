<?php
require_once('oophp/class.Folder.inc.php5');
require_once('oophp/class.Template.inc.php5');

/** Diese Klasse durchläuft das angegebene Verzeichnis und gibt die enthaltenen
 * Verzeichnisse (nur 1. Ebene) als Links-Sammlung (Menü) aus.
 * 
 * @author  Marco Link  
 */
class Menu {
  private static $instance = NULL;
  
  private function __construct() {
  }

  public static function getInstance() {
    if (self::$instance == NULL) {
      self::$instance = new self();
    }
    
    return self::$instance;
  }
  
  public function main(array $args) {
    $dir = new Folder($args['absoluteRootDir']);
    $directories = $dir->getFolders();
    $mainTemplate = new Template();
    $foldernameTemplate = new Template();
    $foldernameTemplatePictures = '';
    $mainTemplateFoldernames = '';

    $mainTemplate->load('../res/templates/menu.inc.html');
    $foldernameTemplate->load('../res/templates/foldername.inc.html');    

    for ($d = 0; $d < count($directories); ++$d) {    
      $foldernameTemplate->setPlaceholderValue('foldername', $directories[$d]->getName());
      $foldernameTemplatePictures = $foldernameTemplate->__toString() . $foldernameTemplatePictures;
    }

    $mainTemplate->setPlaceholderValue('count', count($directories));     
    $mainTemplate->setPlaceholderValue('template.foldernames', $foldernameTemplatePictures);
        
    echo $mainTemplate->__toString();
    
    unset($dir);
    unset($directories);
    unset($mainTemplate);
    unset($foldernameTemplate);
    unset($foldernameTemplatePictures);
    unset($mainTemplateFoldernames);    
  }      
};
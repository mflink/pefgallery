<?php
ini_set('include_path', '.;../shared');

require_once('class.peffile.inc.php5');

if (  isset($_REQUEST['orientation']) 
      && isset($_REQUEST['foldername']) 
      && isset($_REQUEST['filetitle'])) {
  $pef = new PEFFile('P:/PEF/' . $_REQUEST['foldername'] . '/' . $_REQUEST['filetitle'] . '.pef');

  $pef->setOrientation($_REQUEST['orientation']);
  
  if (isset($_REQUEST['goto'])) {
    unset($pef);
    header('Location: ' . $_REQUEST['goto']);
  } else {
    echo 'Orientation of \'' . $pef->getFilename() . '\' is now \'' .  $pef->getOrientation() . '\'';  
    unset($pef);
  }  
}
<?php

error_reporting(E_ERROR | E_PARSE);

require_once('class.peffile.inc.php5');
require_once('oophp/class.Folder.inc.php5');
require_once('oophp/class.Template.inc.php5');

/** Diese Klasse ist ein Singleton und stellt die Hauptmethoden für dieses
 * Programm dar.
 * Dieses Programm durchläuft ein bestimmtes Verzeichnis rekursiv und durchsucht
 * es nach PEF-Dateien (Endung wird geprüft). Findet es eine PEF-Datei, wird
 * das enthaltene Thumbnail in einer HTML-Seite ausgegeben.
 * Diese HTML-Seite stellt dabei eine Galerie zur Betrachtung/Vorschau und zum
 * Herunterladen der JPEG-Versionen ausgewählter PEF-Dateien bereit.
 * 
 * @author  Marco Link
 */
class Content {
  private static $instance = NULL;
  
  private function __construct() {
  }
  
  public static function getInstance() {
    if (self::$instance == NULL) {
      self::$instance = new self();
    }
    
    return self::$instance;
  }
  
  public function main(array $args) {
    $dir = new Folder($args['absoluteRootDir'] . $args['foldername']);
    $mainTemplate = new Template();
    $pictureTemplate = new Template();
    $mainTemplatePictures = '';
    
    if (isset($_REQUEST['expired'])) {
      header('Content-Type: text/html');    
      header('Cache-Control: no-cache, must-revalidate');
      header('Expires: Tue, 30 Dez 1980 00:00:00 GMT');
    } 

    $mainTemplate->load('../res/templates/content.inc.html');
    $pictureTemplate->load('../res/templates/picture.inc.html');    

    $files = $dir->getFiles(true, '(.\.pef)$');
        
    for ($f = 0; $f < count($files);++$f) {
      $pictureTemplate->setPlaceholderValue('filetitle', $files[$f]->getTitle());
      $pictureTemplate->setPlaceholderValue('foldername', $dir->getName());
      
      if (isset($_REQUEST['expired'])) {
        $pictureTemplate->setPlaceholderValue('extraparams', '&expired');
        $pictureTemplate->setPlaceholderValue('extraparamsamp', '&amp;expired');
      } else {
        $pictureTemplate->setPlaceholderValue('extraparams', '');
        $pictureTemplate->setPlaceholderValue('extraparamsamp', '');
      }
      
      $mainTemplatePictures = $mainTemplatePictures . $pictureTemplate->__toString();
    }
    
    $mainTemplate->setPlaceholderValue('foldername', $dir->getName());
    $mainTemplate->setPlaceholderValue('count', count($files));
    $mainTemplate->setPlaceholderValue('template.pictures', $mainTemplatePictures);
   
    echo $mainTemplate->__toString();
    unset($dir);
    unset($files);
    unset($mainTemplate);
    unset($pictureTemplate);            
    unset($mainTemplatePictures);          
  }      
};
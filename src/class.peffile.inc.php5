<?php
require_once('oophp/class.File.inc.php5');
require_once('oophp/class.VectorMap.inc.php5');

/** Diese Klasse stellt Methoden zur Behandlung von PEF-Dateien (Pentax Foto 
 * Dateien) bereit.
 * 
 * @author  Marco Link  
 */
class PEFFile {
  /** Die Positionen der einzelnen, eingebeteten JPEGs. 
   */  
  public static $JPEG_OFFSETS = array(  'medium'  =>  0x00003394, 
                                        'small'   =>  0x0001005e, 
                                        'large'   =>  0x008df4de);
  /** Der Offset zur Orientierungs-Kennung.
   */
  public static $ORIENTATION_OFFSET = 0x00000073;
  /** Die Kennungen für die Orientierung des Bildes.
   * - tb = top-to-bottom
   * - rl = right-to-left
   * - bt = bottom-to-top
   * - lr = left-to-right      
   * - m = mirrored        
   */
  public static $ORIENTATION_ID = array(  0x01  =>  'tb',
                                          0x02  =>  'lrm',
                                          0x03  =>  'lr',
                                          0x04  =>  'rlm',
                                          0x05  =>  'btm',
                                          0x06  =>  'bt',
                                          0x07  =>  'tbm',
                                          0x08  =>  'rl',
                                          'tb'  =>  0x01,
                                          'lrm' =>  0x02,
                                          'lr'  =>  0x03,
                                          'rlm' =>  0x04,
                                          'btm' =>  0x05,
                                          'bt'  =>  0x06,
                                          'tbm' =>  0x07,    
                                          'rl'  =>  0x08);
  /** Die Anzahl auf einmal zu lesender Bytes in extractEmbeddedPicture().
   */
  protected static $BLOCKSIZE = 1024;  

  /** Der Dateiname, der von der Instanz dieser Klasse verwendet werden soll.
   */    
  protected $filename;
                           
  /** Erzeugt ein neues Objekt dieser Klasse.
   *
   * @param filename   Der Pfad und Name der zu verwendenden Datei.     
   */                                                                                         
  public function __construct($filename) {
    $this->filename = $filename;
  }
  
  /** Gibt die Orientierung des Bildes zurück.
   *
   * @returns   'v' für vertikale Orientierung.
   * @returns   'h' für horizontale Orientierung.
   * @returns   Leere Zeichenkette, wenn die Orientierung nicht ermittelt werden
   *            konnte.              
   */
  public function getOrientation() {
    $result = '';
    $file = new File($this->filename);
    
    if ($file->open()) {
      $orientation = $file->read(1, self::$ORIENTATION_OFFSET);
      $orientation = ord($orientation);
      
      if (array_key_exists($orientation, self::$ORIENTATION_ID)) {
        $result = self::$ORIENTATION_ID[$orientation]; 
      }
    }
    
    unset($file);
    
    return $result;    
  }

  /** Legt die Orientierung des Bildes neu fest und schreibt es in die Datei.
   *
   * @param orientation   Der Wert für die Orientierung des Bildes.
   * 
   * @see ORIENTATION_ID
   */    
  public function setOrientation($orientation) {
    if (array_key_exists($orientation, self::$ORIENTATION_ID)) {
      $file = new File($this->filename);
      
      if ($file->open('r+')) {
        if (strlen($orientation) == 1) {
          // Hexa-/Dezimale Angabe wurde getätigt. Kann direkt geschrieben
          // werden.
          $file->write(chr($orientation), 0, self::$ORIENTATION_OFFSET);
        } else {
          // ASCII-Zeichen wurden angegeben. Wert aus dem Array ist gültig.
          $file->write(chr(self::$ORIENTATION_ID[$orientation]), 0, self::$ORIENTATION_OFFSET);
        }
      }
      
      unset($file);
    }
  }  

  /** Extrahiert die angegebene Version eines eingebetteten Bildes.
   *
   * @param  version        Die zu extrahierende Version des eingebetteten 
   *                        Bildes.
   *                        Derzeit sind erlaubt:
   *                        - small für 160px * 120px
   *                        - medium für 640px * 480px
   *                        - large für 3008px * 2000px
   *                        
   * @returns               Das binär-kodierte JPEG des angeforderten Bildes.
   * @returns               NULL bei einem Fehler.
   * 
   * @remarks               Der Speicherbereich in der Rückgabe muss explizit
   *                        freigegeben werden.                            
   */    
  public function extractEmbeddedPicture($version) {
    $result = NULL;
    
    if (array_key_exists($version, self::$JPEG_OFFSETS)) {
      $offset = self::$JPEG_OFFSETS[$version];
      $file = new File($this->filename);
      
      if ($file->open()) {
        $tagBin = $file->read(2, $offset);
        $tag = (ord($tagBin[0]) << 8) + ord($tagBin[1]);
        
        if ($tag == 0xffd8) {
          $result = $result . $tagBin;
          $tagBin = $file->read(2);
          $tag = (ord($tagBin[0]) << 8) + ord($tagBin[1]);
          $lengthBin = $file->read(2);
          $length = (ord($lengthBin[0]) << 8) + ord($lengthBin[1]) - 2;
          $data = ' ';
          
          while (($tag != 0xffda) && (strlen($data) > 0)) {
            $data = $file->read($length);
            
            if (strlen($data) > 0) {
              $result = $result . $tagBin . $lengthBin . $data;
              $tagBin = $file->read(2);
              $tag = (ord($tagBin[0]) << 8) + ord($tagBin[1]);
              $lengthBin = $file->read(2);
              $length = (ord($lengthBin[0]) << 8) + ord($lengthBin[1]) - 2;
            }          
          }
          
          if (strlen($data) > 0) {
            $result = $result . $tagBin . $lengthBin;
            $found = FALSE;
            $data = $file->read(self::$BLOCKSIZE);
            
            while (!$found && (strlen($data) > 0)) {
              $endPos = strpos($data, chr(0xff) . chr(0xd9));
              
              if ($endPos === FALSE) {
                $result = $result . $data;                                
                $data = $file->read(self::$BLOCKSIZE); 
              } else {
                $result = $result . substr($data, 0, $endPos + 2);
                $found = TRUE;
              }
            }            

            unset($data);
          } else {
            unset($result);
            $result = NULL;
          }
        }
      }
      
      unset($file);
    }    
    
    return $result;            
  }
  
  /** Gibt den Dateinamen zurück.
   *
   * @returns   Den Dateinamen.     
   */
  function getFilename() {
    return $this->filename;
  }    
};
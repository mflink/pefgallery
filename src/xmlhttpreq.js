var xmlHttpReq = null;

// Mozilla, Opera, Safari sowie Internet Explorer (ab v7)
if (typeof XMLHttpRequest != 'undefined') {
  xmlHttpReq = new XMLHttpRequest();
}

if (!xmlHttpReq) {
  // Internet Explorer 6 und älter
  try {
    xmlHttpReq  = new ActiveXObject("Msxml2.XMLHTTP");
  } catch(e) {
    try {
      xmlHttpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    } catch(e) {
      xmlHttpReq  = null;
    }
  }
}
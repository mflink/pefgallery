<?php
ini_set('include_path', '.;../shared');

require_once('class.peffile.inc.php5');
require_once('class.jpegdataoutput.inc.php5');

function appendToTextfile($text, $filename) {
  $file = fopen($filename, 'a');
  
  if ($file) {
    fwrite($file, $text);
    
    fclose($file);
  }  
}

if (isset($_REQUEST['foldername']) && isset($_REQUEST['filetitle'])) {
  $pef = new PEFFile('P:/PEF/' . $_REQUEST['foldername'] . '/' . $_REQUEST['filetitle'] . '.pef');
  $version = 'small';
  
  if (isset($_REQUEST['version'])) {
    $version = $_REQUEST['version'];      
  }
  
  $data = $pef->extractEmbeddedPicture($version);
  
  if ($data != NULL) {
    if (isset($_REQUEST['orientation'])) {
      $orientation = $pef->getOrientation();
      
      if ($orientation != 'tb') {
        $angle = 0.0;
        
        switch ($orientation) {
          case 'rl':
              $angle = 90.0;
            break;
          case 'bt':
              $angle = 180.0;
            break;
          case 'lr':
              $angle = 270.0;
            break;
        }
      
        $image = imageCreateFromString($data);

        unset($data);

        $image = imageRotate($image, $angle, 0);
        
        ob_start();
          imageJpeg($image, NULL, 100);
          $data = ob_get_contents();
        ob_end_clean();
        
        imageDestroy($image);
      }
    }
    
    if (isset($_REQUEST['download'])) {
      JPEGDataOutput::send($data, $_REQUEST['filetitle'] . '_' . $version . '.jpeg');
    } else {
      JPEGDataOutput::show($data, isset($_REQUEST['expired']));
    }
  } else {
    echo 'ERROR: No data fetched from ' . $_REQUEST['foldername'] . '/' . $_REQUEST['filetitle'] . '.pef!';
  }

  unset($pef);  
  unset($data);
}
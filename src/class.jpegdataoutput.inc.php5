<?php

/** Diese Klasse stellt Methoden zur Rücksendung eines JPEGs an einen Browser
 * bereit.
 * 
 * @author  Marco Link  
 */
class JPEGDataOutput {
  /** Schickt eine Download-Antwort an den Client zurück.
   *
   * @param data      Die zu sendenden Bilddaten.
   *    
   * @param filename  Der Name für die angebotene Datei.         
   */  
  public static function send($data, $filename) {
    header('Accept-Ranges: bytes');
    header('Content-Length: ' . strlen($data));
    header('Content-Transfer-Encoding: binary');
    header('Content-Type: image/jpeg');
    header('Content-Disposition: attachment; filename="' . $filename . '"');
    
    echo $data;
  }
  
  /** Erzeugt eine Ausgabe der Daten als JPEG.
   *
   * @param data      Die zu sendenden Bilddaten.
   *    
   * @param expired   Bestimmt, ob dem Client eine Meldung zu Neuladen des
   *                  Bildes gesendet werden soll. Ist der Wert false, wird
   *                  der Client evtl. das Bild aus seinem Cache holen.            
   */  
  public static function show($data, $expired = false) {
    header('Accept-Ranges: bytes');
    header('Content-Length: ' . strlen($data));
    header('Content-Transfer-Encoding: binary');
    header('Content-Type: image/jpeg');
    
    if ($expired) {
      header("Cache-Control: no-cache, must-revalidate");
      header("Expires: Tue, 30 Dez 1980 00:00:00 GMT");
    }
    
    echo $data;
  }     
};